\chapter{Adversarial Search}
Adversarial games can be formulated as partially/fully observable, adversarial environment, where both players try to maximize its performance measure while trying to minimize opponents. Optimal play in this kind of games requires playing the move which not only gives the best immediate outcome but rather maximizes possibility of winning.

\section{Minimax Search}

Minimax search is the most commonly encountered method to play games optimally. A game can be thought of as a tree of possible future game states. For example, in Tic-Tac-Toe the game state is the arrangement of the board. The current state of the game is the root of the tree. In general this node has several children, representing all of the possible moves that we could make. Each of those nodes has children representing the game state after each of the opponent's moves. These nodes have children corresponding to the possible second moves of the current player, and so on. The leaves of the tree are final states of the game: states where no further move can be made because one player has won, or perhaps the game is a tie. Actually, in general the tree is a graph, because there may be more than one way to get to a particular state. In some games (for example checkers) it is even possible to revisit a prior game state. To know more one can see \cite{Russel}.

Suppose that we assign a value of positive infinity to a leaf state in which we win, negative infinity to states in which the opponent wins, and zero to tie states. We can define a function Evaluate that can be applied to a leaf state to determine which of these values are correct.

If we can traverse the entire game tree, we can figure out whether the game is a win for the current player assuming perfect play: we assign a value to the current game state by recursively walking the tree. At leaf nodes we return the appropriate values. At nodes where we get to move, we take the max of the child values because we want to pick the best move; at nodes where the opponent moves we take the min of child values. This gives us the following pseudo code for  minimax evaluation of a game tree.
\\

\begin{verbatim}
begin function MINIMAX_DECISION(state) returns an action
    inputs: state, current state in game 
    v := MAX_VALUE(state) 
    return the action in SUCCESSORS(state) with value v  
end function

begin function MAX_VALUE(state) returns a utility value 
    if LEAF_TEST(state) then return EVALUATE(state)
    v := -INFINITY
    for all s in SUCCESSOR(state) do 
        v := MAX(v, MIN_VALUE(s)) 
    return v 
end function

begin function MIN_VALUE(state) returns a utility value 
    if LEAF_TEST(state) then return EVALUATE(state)
    v := +INFINITY
    for all s in SUCCESSOR(state) do  
        v := MIN(v, MAX_VALUE(s)) 
    return v 
end function
\end{verbatim}


Consider the following game tree in figure \ref{fig:bfrminimax}, where the leaves are annotated with W or L to indicate a winning or losing position for the current player (L $<$ W), and interior nodes are labeled with + or - to indicate whether they are ``max'' nodes where we move or ``min'' nodes where the opponent moves. In this game tree, the position at the root of the tree is a losing position because the opponent can force the game to proceed to an ``L'' node.

\begin{figure}[!ht]
\centering
\includegraphics{bfrminimax}
\caption{Game tree, before minimax search}\label{fig:bfrminimax}
\end{figure}

We can see this in figure \ref{fig:aftminimax} by doing a minimax evaluation of all the nodes in the tree. Each node is labeled with its minimax value.

\begin{figure}[!ht]
\centering
\includegraphics{aftminimax}
\caption{Game tree, after minimax search}\label{fig:aftminimax}
\end{figure}

\section{Static Evaluation}

In case of chess or many other games, expanding the entire game tree is infeasible because there are so many possible states. The solution is to search the tree to a specified depth. For this purpose, the evaluate function can be extended so that it returns a value between win or lose for game positions that are not final positions. For game positions that look better for the current player, it returns larger numbers. When the depth limit of the search is exceeded, the static evaluator is applied to the node as if it were a leaf.

The pseudo code for minimax will look something like the following:
\\
\begin{verbatim}
begin function MINIMAX_DECISION(state, depth_limit) returns an action
    inputs: state, current state in game 
    v := MAX_VALUE(state, depth_limit) 
    return the action in SUCCESSORS(state) with value v 
end function

begin function MAX_VALUE(state, depth_limit) returns a utility value 
    if depth_limit=0 or LEAF_TEST(state) then return EVALUATE(state)
    v := -INFINITY
    for all s in SUCCESSOR(state) do 
        v := MAX(v, MIN_VALUE(s, depth_limit-1)) 
    return v 
end function

begin function MIN_VALUE(state, depth_limit) returns a utility value 
    if depth_limit=0 or LEAF_TEST(state) then return EVALUATE(state)
    v := +INFINITY 
    for all s in SUCCESSOR(state) do 
        v := MIN(v, MAX_VALUE(s,depth_limit-1)) 
    return v 
end function
\end{verbatim}

For example, consider the following game tree in figure \ref{fig:dlminimax}, searched to depth 3, where the static evaluator is applied to a number of nodes that are not leaves in the game tree:

\begin{figure}[!ht]
\centering
\includegraphics{dlminimax}
\caption{Game tree, after depth limited minimax search}\label{fig:dlminimax}
\end{figure}

The value of the root of the tree is 6 because the current player can force the game to go to a ``leaf'' node (as defined by the depth cutoff) whose value is at least 6. Notice that by finding out the value of the current position, the player also learns what is the best move to make: the move that transitions the game to the immediate child with maximum value. 

Designing the static evaluator is an art: a good static evaluator should be very fast, because it is the limiting factor in how quickly the search algorithm runs. But it also needs to capture a reasonable approximation of how good the current board position is, because it captures what the player is trying to achieve during play. In practice, game AI designers have found that it doesn't pay to build intelligence into the static evaluator when the same information can be obtained by searching a level or two deeper in the game tree. 

How deeply should the tree be searched? This depends on how much time is available to do the search. Each increase in depth multiplies the total search time by about the number of moves available at each level. 

\section{Pruning the Game Tree}

Even with the depth limit and a very fast evaluation function, the optimality of the game play may not be ensured. The reason behind this is simple. As the search is incomplete, there can be paths which may seem lucrative in the shallower side but may not be as fruitful in the deeper side. Also every board position in the path to a win may not be evaluated as increasing order. Which means, sometimes we may have to weaken ourselves to get stronger in the future.

The solutions to these problems are not straightforward. It can be proved easily that, the deeper we can search, the better we will play. So most algorithms try to find pruning techniques which will reduce the branching factor of the nodes. And as result will increase the depth limit.

In case of pruning a branch out, we need to make sure that we are not taking away a valuable path out of the equation. While pruning we need to make sure that the path we are taking out of consideration is not at all valuable. Alpha beta pruning is one of the most common pruning technique in minimax. This technique is general in the sense that it can be used with any game which can be played using minimax.

The problem with alpha beta pruning is that it may not be helpful for games like chess, where we cannot expand the tree till leaf. And also the successful pruning may not be sufficient to increase the depth limit to a considerable amount. In these kind of cases, we need to think about more of a particular kind of pruning techniques for different games. For Example, we are developing a pruning algorithm for chess in this document.
