\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {1.1}Motivation}{5}
\contentsline {section}{\numberline {1.2}Contribution of our thesis}{6}
\contentsline {section}{\numberline {1.3}Outline of the Thesis}{7}
\contentsline {chapter}{\numberline {2}Related Works and Background Study}{8}
\contentsline {section}{\numberline {2.1}Research Activities Related to Keyword Extraction}{8}
\contentsline {section}{\numberline {2.2}Research Activities Related to Similarity Measurement}{10}
\contentsline {section}{\numberline {2.3}Research Activities Related to Clustering}{16}
\contentsline {subsection}{\numberline {2.3.1}Fuzzy C-Means Clustering}{18}
\contentsline {subsubsection}{\numberline {2.3.1.1}Basic Procedure of FCM}{19}
\contentsline {subsubsection}{\numberline {2.3.1.2}Initial Cluster Centroid Choosing}{20}
\contentsline {subsubsection}{\numberline {2.3.1.3}Updating Cluster Centroid Value}{20}
\contentsline {subsubsection}{\numberline {2.3.1.4}Updating Membership Value}{20}
\contentsline {section}{\numberline {2.4}Research Activities Related to Cluster Validation}{21}
\contentsline {section}{\numberline {2.5}HTML Tags}{24}
\contentsline {chapter}{\numberline {3}Proposed Work}{25}
\contentsline {section}{\numberline {3.1}Text Preprocessing}{25}
\contentsline {subsection}{\numberline {3.1.1}Stop Word Removal}{26}
\contentsline {subsection}{\numberline {3.1.2}Keyword Extraction}{27}
\contentsline {subsection}{\numberline {3.1.3}Assignment of weight}{28}
\contentsline {subsubsection}{\numberline {3.1.3.1}Frequency of the keyword}{28}
\contentsline {subsubsection}{\numberline {3.1.3.2}Position of the keyword}{28}
\contentsline {subsubsection}{\numberline {3.1.3.3}Value of weight}{29}
\contentsline {subsubsection}{\numberline {3.1.3.4}Filtering less weighted keywords}{31}
\contentsline {section}{\numberline {3.2}Similarity Computation}{33}
\contentsline {section}{\numberline {3.3}Clustering of Web Document}{39}
\contentsline {chapter}{\numberline {4}Performance Evaluation}{40}
\contentsline {section}{\numberline {4.1}Dataset and Experimental Setup}{40}
\contentsline {section}{\numberline {4.2}Factors for measuring performance}{41}
\contentsline {section}{\numberline {4.3}Percentage of document duplications}{41}
\contentsline {section}{\numberline {4.4}Evaluation of Proposed Work}{42}
\contentsline {subsection}{\numberline {4.4.1}With respect to Number of Clusters}{42}
\contentsline {paragraph}{Observation}{42}
\contentsline {subsection}{\numberline {4.4.2}With respect to position weight}{45}
\contentsline {paragraph}{Observation}{46}
\contentsline {subsection}{\numberline {4.4.3}With respect to frequency and weight threshold}{47}
\contentsline {paragraph}{Observation}{47}
\contentsline {subsection}{\numberline {4.4.4}With respect to Initial Cluster Choosing Method}{48}
\contentsline {paragraph}{Observation}{49}
\contentsline {chapter}{\numberline {5}Conclusion}{51}
\contentsline {section}{\numberline {5.1}Conclusion}{51}
\contentsline {section}{\numberline {5.2}Future Improvements}{52}
\contentsline {chapter}{\hbox to\@tempdima {\hfil }References}{53}
